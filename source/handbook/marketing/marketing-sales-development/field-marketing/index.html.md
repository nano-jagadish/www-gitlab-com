---
layout: markdown_page
title: Field Marketing
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Field Marketing

Field marketing includes events, event marketing, sponsorships, and swag production. The role of field marketing is to identify and support critical sales and marketing opportunities through field marketing avenues and to bring the GitLab brand to life.

## Field Events   

To find out what upcoming events GitLab and our resellers will be involved in please visit our [Events Page](https://about.gitlab.com/events/). If you have any questions or an event suggestion for us please email `events@gitlab.com`.

### Field Event Goals

- Sales Acceleration
    - Engaging with existing customers
    - New growth opportunities
- Demand
    - Education
- Market Intelligence
    - Test out new messaging or positioning
    - Product Direction
- Brand
    - Awareness
    - Thought Leadership
    - Evangelism - Talk to as many people as we can about GitLab to drive awareness. Our presence should be friendly, knowledgeable, and genuine. We are all the brand and we want to bring that brand to life and be memorable.
- Hiring - Always be recruiting. See someone doing a great job of evangelism for another product? Ask that person to grab a coffee.
- Partnership - Organizations adding support for GitLab and/or shipping GitLab with their offering.

### Event Lifecycle    

#### Evaluating Potential Field Initiatives

- Price/ Budget and what will the ROI be.
- Location - top cities for developers and can we tag on another event or customer meetings?
- Attendees - who’s attending? Is it the right audience for us? Enterprise vs Community. Will there be decision makers in attendance?
- Size - try to reach a large audience.
- What will our presence be? Who can/ can we go, participate, speak, booth?
- We want to emphasize enterprise events but also remember our community.

##### For GitLabbers Attending Events/ Speaking    

- If you are interested in find out about speaking opportunities join the #CFP channel. Deadlines for talks can be found here and also in the GitLab [events spreadsheet](https://docs.google.com/spreadsheets/d/16usWToIsD-loDQYpflaMiGTmERMYSieNj_QAuk5HBeY/edit#gid=1939281399).
- If you want help building out a talk or coming up with ideas for a speaking opportunity email marketing@gitlab.com with some of your expertise and topics and events that may be of interest to you. We are happy to help in anyway we can, including public speaking coaching.
- If there is an event you would like to attend, are attending, speaking, or have proposed a talk and you would like support from GitLab to attend this event the process goes as follows:
 1. Contact your manager for approval to attend/ speak.
 1. After getting approval from your manager, please add your event to the [events page](https://about.gitlab.com/events/) and submit merge request to Emily Kyle.
 1. If you would like additional GitLab materials such as swag please see notes on swag lower on this page.

##### Speaker Portal    

Catalogue of talks, speaker briefs and speakers can be found on our [Find a Speaker page](https://about.gitlab.com/find-a-speaker/). Feel free to add yourself to this page and submit a MR if you want to be in our speaker portal and are interested in being considered for any upcoming speaking opportunities.

##### Suggesting We Sponsor an Event  

- If there is an event or conference you think would be a perfect fit for us to get involved in or sponsor based on the "evaluating and event" criteria above, the process for submitting that suggestion goes as follows:
 1. Please *only request* event support/ sponsorship if your engagement fits the following criteria:
		1. The event will further business aims of GitLab (see event goals).  
	    2. The event has an audience of **250+ people** (the exception being meet-ups - see below for more info on meet-ups)
	    3. The event is a more than a month away.
1. If your event fits the criteria above and you would like support from marketing, create an issue in the [marketing project](https://gitlab.com/gitlab-com/marketing/issues) using the Events template.
1. Fill out any relevant information you have and tell us what potential growth opportunities can be gained from investing in this event. Someone from Marketing will be in touch about to discuss your request further within one week.

##### How We Decide Who Attends Which Events?

* Determine how many staffers we need and if the event is more enterprise focused or community. Enterprise- we try to send more marketing/ sales. Regional Sales Managers select staffer based on who has most potential contacts in area or going to event.  Community - send more developers, subject matter experts, and dev advocates.  
* See who is in the area who might be a good fit for the audience.
* We lean towards those who might be thought leaders, specialists, or more social in that specific sector.
* We aim to bring minimal staff to keep costs and disruption to normal workflow low.

##### Where can I find out more about how to speak at an event?

- Call for papers are posted in the `#CFP` Channel on Slack. Follow channel for updates on speaking opportunities.
- If you want like to speak somewhere and would like assistance building our your proposal please contact `content@gitlab.com`.

#### Pre-Event   

##### Promoting Events Social

- [Use social media](/handbook/marketing/social-marketing/#event-promotion) to post as soon as committed to attending an event.
- Possible Targeted Social Ads (media source depends on target audience and attendance goals). Timing depends on call to action.
- Schedule tweets one week before event and one to go out at event promoting presences and any relevant content.

##### Event Outreach

- Email signatures - starting 6 to 4 weeks before event for those attending event and XDR team.
- Get attendee list and contact customers and prospects before event using talking points from content with goal of setting up meetings/ demos at the event. Invite them to anything specific we have happening at or around event.
- If there is not an attendee list do "warm" outreach to prospects in event location or area of interest. Invite them to lunch or coffee or to booth at event.
- SDRs/BDRs will help set up in person meetings to occur at event in coordination with assigned account reps.

##### Event Content/ Design

- Assess individual event content needs. Most events will need...
     -  Social copy + design
     -  Social ad or InMail copy + design
     -  Swag designs
     -  Pre and post event talking points for sales outreach
     -  After event wrap up email(s)
     -  Additional blog or content to support event strategy
     -  Updated one pagers
     -  Targeted slide decks and demos for specific event audience

##### Event Operations Checklist Before Event

- Add event to [events page](https://about.gitlab.com/events/)
- Create issue for event in Marketing Project on gitlab.com (use events issue template).
- Set up event campaign in Salesforce. The campaign must include event budget and goals for # of leads and # SCLAU. Other goals should be established and documented in the campaign for # meetings at event, and attendance at planned activities such as VIP dinners, lunches, etc., as applicable.
- Team should add members to campaign to be invited to event, using the appropriate `Campaign Status`:   
     - `Invited` = You have personally invited the person to the event.
     - `Nominated` = Marketing will invite the person on your behalf. They will receive invitation email pre-event plus any confirmations/reminders if they register.
     - `Registered` = The system will automatically update to this status once someone completes they form on the Registration Landing page. This is a **system applied status** not for manual additions to a Campaign.
- Set up post event follow up campaigns in Marketo.
- After Event Survey created and email template ready to go out first work day back after event. Includes details on lead followup and how to add business card details.
- Social media to go out during event scheduled.
- Plan for what to do with any remaining swag/ collateral.
- Final Event Team prep meeting to go over talking points, assets, and scheduling.

#### At Events/ In the Field
- For events where a field morketing representative cannot be present, he/she will assign an onsite lead. The FM manager will be responsible for coordinating with this person and getting them any info they will need to help run event in their absence. This person will be the venue point of contact as well as responsible for set up and tear down. They will also be in charge of ordering swag from the "small event swag campaign"- see swag section below. A list of their specific responsibilites will be found in the designated event issue.  

##### Employee Booth Guidelines

- If you see someone standing alone, talk to them. Listen. Ask questions. Don't interrupt.
- Take notes on encounters in the notes field of lead scanner. Add your initials to notes so we know who spoke with the person when we do followup.
- Do not stand around and talk to other GitLab coworkers. Talk to people you don’t know.
- Do not sell; generate interest to learn more. Attendees have a lot of info they are digesting, so get their info and some key info to follow up on.
- Give out swag and documentation we have!
- During slow times, don’t be afraid to walk around to other booths and talk to people. Make friends we could partner with, create interesting content with, or just have friendly beers.
- The booth should be clean and organized at all times.
- Document any product feedback you get.
- If press comes to the event feel free to put them in contact with CMO.
- Bring your business cards and a plan to take notes on encounters.
- Avoid eating meals in booth, please keep lids on beverages and keep them out of sight.
- If a conversation is running long, get his/ her info and schedule a time to chat or follow up at a later time outside of the booth. The goal of the booth is to make initial contact and connections.

##### Suggested Attire   

- Wear at least one piece of branded GitLab clothing. If you prefer to wear something dressier than the GitLab branded items available that is also acceptable. Feel free to wear our sticker on your shirt.
- If the conference is business casual try some nice jeans (no holes) or dress pants.
- Clean, closed-toed shoes please.
- A smile.

##### Booth Set Up   

- To Bring:
    - Generic business cards
    - Stickers + any other swag
    - Events laptop (for slide show) + charger + dongles
    - Backup power banks
    - Gum
    - One pagers + cheat sheets

##### Booth Staffing    
- Ideally booth shifts will be around 3 hours or less.
- Staff more people during peak traffic hours.
- Avoid shift changes during peak hours.
- Aim to staff booth with individuals with a variety of expertise and backgrounds- ideally technical and non-technical people from various departments should be paired.
- Send out invites on the Events & Sponsorship calendar to booth staff with the following information:
    - Time and date of event, booth, and shift
    - Suggested attire
    - How he/she can find his/her ticket
    - Any instructions on using or locating lead scanner
    - Let them know of any contests happening
    - How to get a hold of you
    - Link to events handbook
    - Any relevant event set up or clean up

#### Post Event   

- Add event debrief to event issue in marketing project. The debrief should include the following if applicable:
    - Was the event valuable?
        - Would you go again/ should we go again?
        - Did we get good leads/ contacts? What was the audience profile like?
        - Best questions asked and conversations. Trends in questions asked.
        - Was our sponsorship/ involvement successful? Did we go in at the proper sponsorship level?
    - How was the booth set up?
        - How was the booth staffing?
        - Did the booth get enough traffic?
        - Booth location and size
    - How did our swag go over?
        - Did we have enough/ too much?
    - Contests
        - Did the contest(s) effectively build our brand and connecting with our target audience?
- Send all leads and contacts with any relevant notes on interactions/necessary follow up to Field Marketing Manager for your Region within 36 hours of event close. 
- When the list is received from conference, the Sales team will have 24 business hrs to review and claim leads, prior to list upload. If for any reason you cannot meet this deadline ping marketing operations or your field marketing rep.
- List upload will be done w/in 48hours of list being received by conference organizers. All leads will be added to corresponding SalesForce campaign as contacts and either get the status of "visited booth" or "requested info". 
  - Notes on upload: When the list is uploaded, if any new lead is associated to a known account they will be assigned to that account owner, but if the lead does not belong to any known account they will assigned based on region where they will be round robined to AE/AMs. The owner will be notified when an assigment is made. 
  - The account SDR will handle and doument followup (within one week of list upload) and pass on lead to AE/AM when appropriate. 
  - If the AE/AM would like to do any personalised outreach to event leads/ contacts they will be responsible for corrdinating efforts with SDR and documenting any activity and updating statuses in sfdc. 
- Marketing will send out one email to all contacts in campaign (within 3 business days of event close). This will be in addition to any SDR touch points that are happening. 
- FM campaign owner will check in on campaign and lead status one week, 2 weeks, one month, and 3 months out from event assisting in any additonal nurturing efforts to move the needle forward. 

## Meet-ups    

- We love and support Meet-ups. If you would like someone from the GitLab team to stop by your event or might be interested in having GitLab as a sponsor please email community@gitlab.com. Please note, we will be much more able to assist if given sufficient lead time (at least a month) to evaluate request, process payment, and produce and swag.
- GitLab Meet-ups: Ideally, the first couple meet-ups should be run by GitLab employees, but once someone manages to have a couple successive events, the meet-up itself will live on. It is much harder to start new meet-ups versus maintaining existing ones. So we make an effort to support and keep existing events going.

## Swag

* We aim to have our swag delight and/ or be useful.
* We aim to make limited edition and themed swag for the community to collect. Bigger events will have custom tanuki stickers in small runs, only available at their specific event.
* We aim to do swag in a way that doesn't take a lot of time to execute => self serve => [web shop](https://gitlab.myshopify.com/)
* We get a lot of requests to send swag, and we try to respond to them all. If you would like to get some GitLab swag for your team or happening, please see below for more info on submitting a swag. Note: We recommend you request swag at least 4 weeks out from the event date or we may not be able to accommodate your request.
    - Community requests: Email your request to sponsorships@gitlab.com. In your request please include the expected number of guests, the best shipping address, and phone number along with what kind of swag you are hoping for. The swag we have available can be found on our online store.
    - Internal GitLab swag ordering:
       -  To request additional GitLab materials for an event you are attending see instructions below.
          -  The event in questions must be 4 or more weeks away for all swag and material requests, as rush shipping is not an option using the link below.
          -  You can place event swag orders with [this link](https://get.printfection.com/ybddf/6351118868). This link can only be accessed via your gitlab.com email address. The request will be approved on the back end by the Field Marketing team. If you do not see what you are looking for in this portal contact events@gitlab.com.
          -  For printed materials (one pager, cheat sheets) please email events@gitlab.com.
       -  For customer/ prospect swag anyone with access to Salesforce can send swag through Salesforce directly. Please review [sending swag to customers paramaters](https://gitlab.com/gitlab-com/sales/issues/144). Instructions on how to do so below:
       -  Step 1: Create general task for whichever contact you want to send swag to. You need to create a task and save it before the "order swag" button will show up as an option in the task menu.
       -  Step 2: Go into created and saved task and click the button that says "order swag".
          - If you do not see the window pop up for ordering, please check your pop up settings and verify pop ups are not being blocked.   
       -  Step 3: Previous step will bring up a printfrection page where you can order desired swag. (please note not all swag is listed in sfdc, for more options email emily@gitlab.com).
       -  For orders of stickers in a quantity of 100 or greater, do not go through the swag store but rather use our [Stickermule](https://www.stickermule.com/) account or ping emily@gitlab.com. You will need the same information (address and order quantity). After placing the order, add the amount to the marketing budget spreadsheet.
* GitLabbers- if you would like to order something from the GitLab swag shop we have a discount code you can use for 30% off. Please see the swag slack channel to get code to be used in the [store](https://shop.gitlab.com/) at checkout. It can be found in the channel description.
* We have GitLab stationary/ note cards- leave note in swag slack channel of you would like a batch to send notes to prospects/ customers/ community members.

* _NOTE:_ Please keep in mind the [list of countries we do not do business in](/handbook/sales-process/images_sales_process/#export-control).

## Terminus Campaigns

- What is terminus and when can I use it?
  - Terminus is an ABM hub that uses banner ads to target people who work at specific companies of our choosing, with the goal of creating general awareness and pushing specific content. It is best used paired with other field strategies to get our foot in the door and build awareness. Not usually used on in isolation to close an opportunity, but can be used to influence it.
  - Creates awareness in specific companies. This tool integrates with SalesForce, so it can only pull from existing accounts in the system. It is best used to target strategic and large accounts. It can target an industry, specific business, or even a specific department in that company. We can get pretty granular with targeting- please reach out for specific requests and questions to events@gitlab.com.
- How to suggest a terminus campaign?
  - Go to the marketing project and find the terminus issue template. Add in all relevant information. Let us know what companies, industries, or sectors you are trying to target when and paired with what larger field presence.
